# Qml Material Slideshow #

This is a template to make your own presentations in Qml, with the Material Design Guideline.
The goal is not to provide a slideshow application, if so go for Google Docs.
This is a basic Qml Slideshow application using Qml Material Elements so you can be free to add as much Qml coolness into it!!!

### Dependencies and credit ###

* [Qt/Qml](http://qt.io)
* [Qml Material](http://papyros.io)

### How to run it? ###

Open the .qmlproject file into Qt Creator IDE.
Click on Launch.

### Features ###

* To make your own slide copy the intro/ folder and rename it to what you want, then add the entry name in the slides
 array
* Fullscreen available with F key
* By default will take size of screen
* Color theme button and config
* Counter with animation
* Move through slides using 'SPACEBAR' and 'ARROW' keys
* See all that Qml can give you http://qt.io http://qmlbook.org


### Screenshots ###

[![QMS in action](http://pix.toile-libre.org/upload/img/1450135368.png)](http://pix.toile-libre.org/upload/original/1450135368.png)

[![QMS in action webview](http://pix.toile-libre.org/upload/img/1450135390.png)](http://pix.toile-libre.org/upload/original/1450135390.png)