import QtQuick 2.5
import QtQuick.Window 2.2
import Material 0.1
import "components"

Presentation {
    id: mainWindow
    title: "material Slideshow"

    theme {
        primaryColor: Palette.colors["blue"]["500"]
        primaryDarkColor: Palette.colors["blue"]["700"]
        accentColor: Palette.colors["blue"]["500"]
        tabHighlightColor: "white"
    }

    ColorPicker {
        id: colorPicker
    }

    initialPage: slide(0)

    slides: [
        "intro",
        "abstract",
        "slide_with_pictures",
        "slide_with_text",
        "side_with_web",
        "credits",
        "end"
    ]

}
