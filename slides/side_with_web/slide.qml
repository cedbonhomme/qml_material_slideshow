import QtQuick 2.5
import Material 0.1
import QtWebKit 3.0
import "../../components"

Slide {
    id: page
    title: "Qt is awesome"


    SlideView {
        id: slideView

        WebView {
                id: webview
                url: "http://qt.io"
                anchors.fill: parent
                focus: false
                onActiveFocusChanged:{
                    //hack to never give focus to webview
                        webview.focus = false
                    }
                onNavigationRequested: {
                    // detect URL scheme prefix, most likely an external link
                    var schemaRE = /^\w+:/;
                    if (schemaRE.test(request.url)) {
                        request.action = WebView.AcceptRequest;
                    } else {
                        request.action = WebView.IgnoreRequest;
                    }
                }
        }
        SlideCounter {
            id: slideCounter
            textColor: "white"
        }
    }
}
