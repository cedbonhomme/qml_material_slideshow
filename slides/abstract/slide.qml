import QtQuick 2.5
import Material 0.1
import "../../components"

Slide {
    id: page
    title: "Abstract"


    SlideView {
        id: slideView

        Flickable {
            anchors.fill: parent

            contentWidth: image.width; contentHeight: image.height

            Image {
                id: image
                fillMode: Image.PreserveAspectCrop
                source: Qt.resolvedUrl("assets/abstract.png")
            }
        }
    }
}
