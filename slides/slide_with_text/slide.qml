import QtQuick 2.5
import Material 0.1
import QtQuick.Layouts 1.1
import "../../components"

Slide {
    id: page
    title: "Archlinux"


    SlideView {
        id: slideView

        Flickable {
            anchors.fill: parent

            contentWidth: columnlayout.width
            contentHeight: columnlayout.height
            ColumnLayout {
                id: columnlayout
                anchors {
                    left: parent.left
                    top: parent.top
                    margins: Units.dp(34)
                }
                spacing: Units.dp(100)

                Image {
                    id: image
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("assets/Arch_Linux_logo.png")
                }

                Grid {
                    columns: 2
                    columnSpacing:  Units.dp(100)
                    rowSpacing: Units.dp(100)

                    Label {
                        font.family: "Roboto"
                        text: "Simplicity is the ultimate sophistication. — Leonardo da Vinci"
                        font.pixelSize: Units.dp(34)
                    }

                    Label {
                        font.family: "Roboto"
                        text: "Modernity"
                        font.pixelSize: Units.dp(34)
                    }

                    Label {
                        font.family: "Roboto"
                        text: "Correctness is clearly the prime quality"
                        font.pixelSize: Units.dp(34)
                    }

                    Label {
                        font.family: "Roboto"
                        text: "User centrality"
                        font.pixelSize: Units.dp(34)
                    }

                    Label {
                        font.family: "Roboto"
                        text: "Versatility"
                        font.pixelSize: Units.dp(34)
                    }

                }

            }

        }
    }
}
