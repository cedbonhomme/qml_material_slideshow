import QtQuick 2.5
import Material 0.1
import "../../components"

Slide {
    id: page
    title: "Title - Author"

    actions: [
        Action {
            iconName: "image/color_lens"
            name: "Colors"
            onTriggered: colorPicker.show()
        }
    ]

    AwesomeIcon {
        name: "globe"
        anchors.right: title.left
        anchors.top: title.top
        size: Units.dp(56)
        color: Palette.colors["blue"]["500"]
    }

    Label {
        id: title
        font.family: "Roboto"
        text: "Title"
        font.pixelSize: Units.dp(56)

        anchors.centerIn: parent
    }
}
