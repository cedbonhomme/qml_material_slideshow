import QtQuick 2.5
import Material 0.1
import Material.ListItems 0.1 as ListItem
import Material.Extras 0.1
import "../../components"

Slide {
    id: page
    title: "Credits"

    SlideView {
        id: slideView

        Flickable {
            anchors.fill: parent
            anchors.margins: Units.dp(34)

            //            contentWidth: image.width; contentHeight: image.height
            Column {
                spacing: Units.dp(100)

                Label {
                    font.family: "Roboto"
                    text: "Qt - http://qt.io"
                    font.pixelSize: Units.dp(24)
                }
                Label {
                    font.family: "Roboto"
                    text: "Qml Material - http://papyros.io/"
                    font.pixelSize: Units.dp(24)
                }
                Label {
                    font.family: "Roboto"
                    text: "Qt5 Cadaques - http://qmlbook.org"
                    font.pixelSize: Units.dp(24)
                }
            }
        }
    }
}
