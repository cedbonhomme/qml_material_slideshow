import QtQuick 2.5
import Material 0.1

View {
    id: slideView
    elevation: 1
    anchors {
        fill: parent
        margins: Units.dp(32)
    }
    
    SlideCounter {
        id: slideCounter
    }
}
